#include "settings.h"

double FitFunc(double *x, double*par){
  double fitval = par[0] +
    par[1]*(            par[2]*TMath::Sin(2.*x[0]*par[8]*TMath::Pi()/par[3]+par[4]) +
	    (1.-par[2]-par[6])*TMath::Sin(2.*x[0]*par[8]*TMath::Pi()/par[5]+par[4]) +
	                par[6]*TMath::Sin(2.*x[0]*par[8]*TMath::Pi()/par[7]+par[4])
	    );

  return fitval;
}

double FitFunc_2(double *x, double*par){
  double fitval = par[0]+
    par[1]*TMath::Cos(2*TMath::Pi()*2.*par[2]*x[0]/par[3] + par[4]) +
    par[5]*TMath::Cos(2*TMath::Pi()*2.*par[2]*x[0]/par[6] + par[4]) +
    par[7]*TMath::Cos(2*TMath::Pi()*2.*par[2]*x[0]/par[8] + par[4]);

  return fitval;
}



void Fitting(TString RootFileName){

  TFile* RootFile = new TFile(RootFileName, "read");
  TTree* lab_tree = (TTree*)RootFile -> Get("lab_tree");

  double measures[SAMPLES_PER_EVENT]; //V
  double source[SAMPLES_PER_EVENT];   //V
  double current[SAMPLES_PER_EVENT];  //A
  double measures_mean = 0.;
  double source_mean = 0.;
  double current_mean = 0.;
  double measures_mean_std_dev = 0.;
  double source_mean_std_dev = 0.;
  double current_mean_std_dev = 0.;
  
  
  lab_tree->SetBranchAddress("Measures",               measures);
  lab_tree->SetBranchAddress("Source",                 source);
  lab_tree->SetBranchAddress("Current",                current);
  lab_tree->SetBranchAddress("Measures_Mean",          &measures_mean);
  lab_tree->SetBranchAddress("Source_Mean",            &source_mean);
  lab_tree->SetBranchAddress("Current_Mean",           &current_mean);
  lab_tree->SetBranchAddress("Measures_Mean_Std_Dev",  &measures_mean_std_dev);
  lab_tree->SetBranchAddress("Source_Mean_Std_Dev",    &source_mean_std_dev);
  lab_tree->SetBranchAddress("Current_Mean_Std_Dev",   &current_mean_std_dev);

  unsigned int NumberOfEvents = lab_tree->GetEntries(); //variabile di lavoro

  TCanvas* interferenza_canvas = new TCanvas("interferenza_canvas", "Interferenza");

  double meas[700];
  double sour[700];
  double meas_std[700];
  double sour_std[700];
  
  for(int k=0; k<700; k++){
    lab_tree->GetEntry(k);
    int i=k;
    meas[i]=measures_mean;
    sour[i]=source_mean;
    meas_std[i]=measures_mean_std_dev+0.01;
    sour_std[i]=source_mean_std_dev*0.;
    
  }
  
  TGraphErrors* interferenza = new TGraphErrors(700,sour,meas,sour_std,meas_std);
  TF1* modello = new TF1("modello", FitFunc, 0., 140., 9);
  modello->SetParNames("Offset","Amplitude","Weight_left","Wavelenght_left","Phase",
		       "Wavelenght_mid","Weight_right","Wavelenght_right", "nm/V");
  modello->SetParameter(0,0.23);
  modello->SetParLimits(0,0.2,0.3);
  modello->SetParameter(1,0.11);
  modello->SetParLimits(1,0.10,0.14);
  modello->SetParameter(2,0.1);
  modello->SetParLimits(2,0.,0.5);
  modello->SetParameter(3,660.);
  modello->SetParLimits(3,655.,665.);
  modello->SetParameter(4,2.89);
  modello->SetParLimits(4,0.,2*TMath::Pi());
  //modello->SetParameter(5,1.);
  //modello->SetParLimits(5,0.9,1.);
  modello->SetParameter(5,670.);
  modello->SetParLimits(5,665.,675.);
  modello->SetParameter(6,0.8);
  modello->SetParLimits(6,0.,0.5);
  modello->SetParameter(7,678.);
  modello->SetParLimits(7,675.,680.);
  modello->SetParameter(8,65.);
  modello->SetParLimits(8,60.,70.);

  TF1* modello_2 = new TF1("modello_2", FitFunc_2, 0., 140., 9);

  modello_2->SetParName(0,"A [V]");
  modello_2->SetParLimits(0,0.22,0.26);
  modello_2->SetParameter(0,0.24);
  
  modello_2->SetParName(1,"B_{1} [V]");
  modello_2->SetParameter(1,0.03);
  modello_2->SetParLimits(1,0.001,0.05);

  modello_2->SetParName(5,"B_{2} [V]");
  modello_2->SetParameter(5,0.077);
  modello_2->SetParLimits(5,0.03,0.13);
  
  modello_2->SetParName(7,"B_{3} [V]");
  modello_2->SetParameter(7,0.018);
  modello_2->SetParLimits(7,0.001,0.05);
  
  modello_2->SetParName(2,"[nm/V]");
  modello_2->SetParLimits(2,20.,40.);
  modello_2->SetParameter(2,32.);
  
  modello_2->SetParName(4,"phase");
  modello_2->SetParameter(4,1.63);
  modello_2->SetParLimits(4,0.,2*TMath::Pi());

  modello_2->SetParName(3,"#lambda_{1} [nm]");   
  modello_2->SetParameter(3,661.);
  modello_2->SetParLimits(3,660.,670.);
  modello_2->SetParName(6,"#lambda_{2} [nm]"); 
  modello_2->SetParameter(6,672.);
  modello_2->SetParLimits(6,665.,675.);
  modello_2->SetParName(8,"#lambda_{3} [nm]"); 
  modello_2->SetParameter(8,673.);
  modello_2->SetParLimits(8,670.,678.);
  

  interferenza->Fit("modello_2", "", "", 30., 100.);
  gStyle->SetOptFit(111);
  gStyle->SetOptStat(11);

  interferenza->GetXaxis()->SetTitle("Source Voltage [V]");
  interferenza->GetYaxis()->SetTitle("ADC measure [V]");
  interferenza->SetTitle("Piezo Mirror interference - run 2a");

  interferenza->SetFillColor(4);
  interferenza->SetFillStyle(3001);
  interferenza->Draw("a3");
  /* 
  printf("|   Name   |   Value  |   Error  |\n");
  printf("| -------- | -------- | -------- |\n");
  for(int l=0; l<9; l++){
    printf("| %s | %.6e | %.6e |\n",
	   modello_2->GetParName(l),
	   modello_2->GetParameter(l),
	   modello_2->GetParError(l)
	   );
  }
  */
  
  TCanvas *conv_volt_canvas = new TCanvas("conv_volt_canvas","Isteresi");
  conv_volt_canvas->cd();
  TGraphErrors *conv_volt_salita = new TGraphErrors(8);
  TGraphErrors *conv_volt_discesa = new TGraphErrors(8);

  conv_volt_salita->SetLineColorAlpha(kBlue, 1);
  conv_volt_salita->SetName("salita");
  conv_volt_salita->SetTitle("da 0V a 140V");
  conv_volt_discesa->SetLineColorAlpha(kRed, 1);;
  conv_volt_discesa->SetName("discesa");
  conv_volt_discesa->SetTitle("da 140V a 0V");

  
  double increment=17.5;
  int n_point=0;
  for (double z=0; z<140.; z+=increment){
    printf("\n\n intervallo di fit: %f %f\n", z, z+increment);
    interferenza->Fit("modello_2", "0", "", z, z+increment);
    //conv_volt_salita->SetPoint(n_point, z+increment/2., modello_2->GetParameter(2)*(z+increment/2.));
    //conv_volt_salita->SetPointError(n_point, 0., modello_2->GetParError(2)*(z+increment/2.));
    printf("| da [V] | a [V] | p [nm/V] | error [nm/V] | chi_square |\n"  );
    printf("| %.1e | %.1e | %.6e | %.6e | %.6e |\n",
	   z, z+increment, modello_2->GetParameter(2), modello_2->GetParError(2),
	   modello_2->GetChisquare() );
    conv_volt_salita->SetPoint(n_point, z+increment/2., modello_2->GetParameter(2));
    conv_volt_salita->SetPointError(n_point, 0., modello_2->GetParError(2));
    n_point++;
  }
 
  
  for(int k=700; k<1400; k++){
    lab_tree->GetEntry(k);
    int i=k-700;
    meas[i]=measures_mean;
    sour[i]=source_mean;
    meas_std[i]=measures_mean_std_dev+0.01;
    sour_std[i]=source_mean_std_dev*0.;
    
  }
  
  delete interferenza;
  interferenza = new TGraphErrors(700,sour,meas,sour_std,meas_std);

  n_point =0;
  for (double z=140.; z>0.; z-=increment){
    printf("\n\n intervallo di fit: %f %f\n", z-increment, z);
    interferenza->Fit("modello_2", "0", "", z-increment, z);
    //conv_volt_discesa->SetPoint(n_point, z-increment/2., modello_2->GetParameter(2)*(z-increment/2.));
    //conv_volt_discesa->SetPointError(n_point, 0., modello_2->GetParError(2)*(z-increment/2.));
    printf("| da [V] | a [V] | p [nm/V] | error [nm/V] | chi_square |\n"  );
    printf("| %.1e | %.1e | %.6e | %.6e | %.6e |\n",
	   z, z-increment, modello_2->GetParameter(2), modello_2->GetParError(2),
	   modello_2->GetChisquare() ); 
    conv_volt_discesa->SetPoint(n_point, z-increment/2., modello_2->GetParameter(2));
    conv_volt_discesa->SetPointError(n_point, 0., modello_2->GetParError(2));
   
    n_point++;
  }


  TMultiGraph *mg = new TMultiGraph("mg","mg");
  
  //mg->SetTitle("Isteresi");
  mg->SetTitle("Variazione fattore di conversione");
  //mg->GetYaxis()->SetTitle("p * Voltage [nm]");
  mg->GetYaxis()->SetTitle("p [nm/V]");
  mg->GetXaxis()->SetTitle("Voltage [V]");
  mg->Add(conv_volt_discesa,"pl");
  mg->Add(conv_volt_salita);
  
  mg->Draw("apl");
  mg->GetYaxis()->SetRangeUser(22.,40.);
  mg->GetXaxis()->SetLimits(0.,140.);
  mg->Draw("apl");
  conv_volt_canvas->Update();
  conv_volt_canvas->BuildLegend();
  
  
  return; 

}
