
void LocalMin_n_Max_search(TString RootFileName){

  TFile* RootFile = new TFile(RootFileName, "read");
  TTree* lab_tree = (TTree*)RootFile -> Get("lab_tree");

  double measures_mean = 0.;
  double source_mean = 0.;
  double current_mean = 0.;
  
  
  lab_tree->SetBranchAddress("Measures_Mean",          &measures_mean);
  lab_tree->SetBranchAddress("Source_Mean",            &source_mean);
  lab_tree->SetBranchAddress("Current_Mean",           &current_mean);
 
  unsigned int NumberOfEvents = lab_tree->GetEntries(); //variabile di lavoro
  
  int max_entry = 0;
  double max_value = 0.25;
  double min_value = 0.25;
  int min_entry = 0;
  int max[1400]={0};
  int min[1400]={0};
  
  for (int m=0; m<NumberOfEvents; m++){

    lab_tree->GetEntry(m);
    
    if( measures_mean >0.28){
      if(measures_mean>max_value) {
	max_entry=m;
	max_value=measures_mean;
      }
    } else if ( measures_mean <= 0.28 && measures_mean >= 0.2){
      max[max_entry]=1;
      min[min_entry]=1;
      min_value=0.25;
      max_value=0.25;
    }else{
      if(measures_mean<min_value){
	min_entry=m;
	min_value=measures_mean;
      }
    }
  }

  double periods[30] = {0.};
  int periods_count =0;
  double previous_value=0.;
  TGraph *periods_graph = new TGraph(27);
 
  for (int n=0; n<NumberOfEvents; n++){
    if (min[n]==1){
      printf("min: %d\n",n);
      lab_tree->GetEntry(n);
      periods[periods_count] = source_mean - previous_value;
      
      if (periods_count!=0) periods_graph->SetPoint(periods_count-1,source_mean,abs(periods[periods_count]));
      
      previous_value = source_mean;
      periods_count++;
    }
    if (max[n]==1){
      printf("max: %d\n",n);
    }
  }

  periods_graph->Draw();

  for (int l=0; l<30; l++){
    printf("%f\n",periods[l]);
  }
  return;
}
