#include "settings.h"

void TreeCreator(string FileName){

  ifstream  InputFile;
  InputFile.open(FileName);
  string RootFileName = FileName.substr(0,FileName.find_last_of('.'))+".root";
  TFile    *RootFile = new TFile( TString(RootFileName),"recreate" );
  
  TTree *tree = new TTree("lab_tree",
			  "Acquisizione dati "+ TString(FileName)
			  );
  
  double measures[SAMPLES_PER_EVENT]; //V
  double source[SAMPLES_PER_EVENT];   //V
  double current[SAMPLES_PER_EVENT];  //A

  double measures_mean = 0.;
  double source_mean = 0.;
  double current_mean = 0.; 

  double measures_mean_std_dev = 0.;
  double source_mean_std_dev = 0.;
  double current_mean_std_dev = 0.; 

  
  tree->Branch("Measures",           measures,                "Measures[30]/D");
  tree->Branch("Source",             source,                  "Source[30]/D");
  tree->Branch("Current",            current,                 "Current[30]/D");

  tree->Branch("Measures_Mean",      &measures_mean,          "Measures_Mean/D");
  tree->Branch("Source_Mean",        &source_mean,            "Source_Mean/D");
  tree->Branch("Current_Mean",       &current_mean,           "Current_Mean/D");

  tree->Branch("Measures_Mean_Std_Dev",  &measures_mean_std_dev,  "Measures_Mean_Std_Dev/D");
  tree->Branch("Source_Mean_Std_Dev",    &source_mean_std_dev,    "Source_Mean_Std_Dev/D");
  tree->Branch("Current_Mean_Std_Dev",   &current_mean_std_dev,   "Current_Mean_Std_Dev/D");

  while(InputFile.good()){
    for(int i=0; i<SAMPLES_PER_EVENT; i++){
      
      InputFile >> measures[i] >> source[i] >> current[i] >> ws;

      measures_mean += measures[i];
      source_mean   += source[i];
      current_mean  += current[i];

    }

    measures_mean = measures_mean/SAMPLES_PER_EVENT;
    source_mean   = source_mean/SAMPLES_PER_EVENT;
    current_mean  = current_mean/SAMPLES_PER_EVENT;    

    for(int j=0; j<SAMPLES_PER_EVENT; j++){

      measures_mean_std_dev += pow(measures_mean - measures[j], 2);
      source_mean_std_dev   += pow(source_mean   - source[j],   2);       
      current_mean_std_dev  += pow(current_mean  - current[j],  2);    

    }

    measures_mean_std_dev = measures_mean_std_dev/TMath::Sqrt(SAMPLES_PER_EVENT*(SAMPLES_PER_EVENT-1));
    source_mean_std_dev   = source_mean_std_dev/TMath::Sqrt(SAMPLES_PER_EVENT*(SAMPLES_PER_EVENT-1));
    current_mean_std_dev  = current_mean_std_dev/TMath::Sqrt(SAMPLES_PER_EVENT*(SAMPLES_PER_EVENT-1));    

    
    tree->Fill();

    source_mean           = 0. ;
    current_mean          = 0. ;
    measures_mean         = 0. ;
    
    source_mean_std_dev   = 0. ;
    current_mean_std_dev  = 0. ;
    measures_mean_std_dev = 0. ;
   }

  
  InputFile.close();
  RootFile->Write();
  
  //puliamo la memoria
  delete tree;
  delete RootFile;

  return;
}


int max_entry = 0;
int min_entry = 0;
    

for (int m=0; m<tree->GetEntries(); m++){

  lab_tree->GetEntry(m);

  if( measures_mean >0.28){
    if(measures_mean>max_entry) max_entry=m;

    } else if ( measures_mean <= 0.28 && measures_mean >= 0.2){

    }else{

    }
 }
