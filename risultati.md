# Interferometro di Michelson e Morley

**Grafici**
1. [diagramma specchio](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/KC1_Kinematic_Mount_Diagram_D1-780.gif)
2. [distribuzione voltaggi intorno allo 0](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/source_histo.pdf)
3. [run 1a: da 30V a 100V](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/run1a_30-100.pdf)

|   Name   |   Value  |   Error  |
| -------- | -------- | -------- |
| A [V] | 2.402449e-01 | 5.367178e-04 |
| B_{1} [V] | 2.689413e-02 | 3.587672e-03 |
| [nm/V] | 3.226015e+01 | 2.522974e-02 |
| #lambda_{1} [nm] | 6.600000e+02 | 2.056546e-01 |
| phase | 1.672878e+00 | 2.189279e-02 |
| B_{2} [V] | 4.593015e-02 | 2.604765e-03 |
| #lambda_{2} [nm] | 6.750000e+02 | 3.111501e-01 |
| B_{3} [V] | 5.000000e-02 | 6.087778e-03 |
| #lambda_{3} [nm] | 6.780000e+02 | 6.341723e-01 |

4. [run 1a: da 30V a 120V](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/run1a_30-120.pdf)

5. [run 1b: da 20V a 50V](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/run1b_20-50.pdf)

|   Name   |   Value  |   Error  |
| -------- | -------- | -------- |
| A [V] | 2.406321e-01 | 8.283098e-04 |
| B_{1} [V] | 2.160997e-02 | 1.676449e-02 |
| [nm/V] | 3.527472e+01 | 2.138771e-01 |
| #lambda_{1} [nm] | 6.600000e+02 | 7.032827e+00 |
| phase | 4.290366e-01 | 3.800175e-02 |
| B_{2} [V] | 8.187491e-02 | 9.355033e-03 |
| #lambda_{2} [nm] | 6.748536e+02 | 4.968470e-01 |
| B_{3} [V] | 1.758920e-02 | 9.984419e-03 |
| #lambda_{3} [nm] | 6.777290e+02 | 7.754665e+00 |


6. [run 2a: da 30V a 100V](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/run2a_30-100.pdf)

|   Name   |   Value  |   Error  |
| -------- | -------- | -------- |
| A [V] | 2.398349e-01 | 5.359691e-04 |
| B_{1} [V] | 3.148246e-02 | 3.320095e-03 |
| [nm/V] | 3.202865e+01 | 2.590235e-02 |
| #lambda_{1} [nm] | 6.600000e+02 | 1.068345e-01 |
| phase | 7.408645e-01 | 2.348885e-02 |
| B_{2} [V] | 4.026574e-02 | 2.499727e-03 |
| #lambda_{2} [nm] | 6.750000e+02 | 1.923719e-01 |
| B_{3} [V] | 5.000000e-02 | 2.842594e-03 |
| #lambda_{3} [nm] | 6.780000e+02 | 2.535444e-01 |

7. [run 2b: da 40V a 90V](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/run2b_40-90.pdf)

|   Name   |   Value  |   Error  |
| -------- | -------- | -------- |
| A [V] | 2.412760e-01 | 6.351459e-04 |
| B_{1} [V] | 3.565957e-02 | 1.681774e-03 |
| [nm/V] | 2.947880e+01 | 3.351504e-02 |
| #lambda_{1} [nm] | 6.600000e+02 | 2.452161e-01 |
| phase | 4.377212e+00 | 3.883581e-02 |
| B_{2} [V] | 3.000000e-02 | 6.783615e-03 |
| #lambda_{2} [nm] | 6.750000e+02 | 6.416108e-01 |
| B_{3} [V] | 5.000000e-02 | 4.899005e-02 |
| #lambda_{3} [nm] | 6.780000e+02 | 4.062630e-01 |

8. [run 2b: da 20V a 50V](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/run2b_20-50.pdf)

|   Name   |   Value  |   Error  |
| -------- | -------- | -------- |
| A [V] | 2.388428e-01 | 8.174602e-04 |
| B_{1} [V] | 4.925477e-02 | 4.725238e-02 |
| [nm/V] | 3.450901e+01 | 2.905805e-01 |
| #lambda_{1} [nm] | 6.676854e+02 | 8.741718e+00 |
| phase | 2.285294e-09 | 1.914679e-03 |
| B_{2} [V] | 6.630149e-02 | 1.722614e-02 |
| #lambda_{2} [nm] | 6.743069e+02 | 8.213352e+00 |
| B_{3} [V] | 5.273628e-03 | 4.057190e-02 |
| #lambda_{3} [nm] | 6.700002e+02 | 7.936095e+00 |

9. [run 1. campionamenti da 0V a 140V e ritorno ](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/run1_entry.pdf)
10. [run 1. sovrapposta ](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/run1_sovrapposta.pdf)
11. [isteresi run1](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/isteresi.pdf)
12. [variazione c run1](https://gitlab.com/thenucciasfourth/interferometro/-/blob/main/risultati/variazione_fattore_di_conversione.pdf)

| da [V]  | a [V]   | p [nm/V]     | error [nm/V] |
| ------- | ------- | ------------ | ------------ |
| 0.0e+00 | 1.8e+01 | 2.436712e+01 | 3.691365e-01 |
| 1.8e+01 | 3.5e+01 | 3.063225e+01 | 8.247673e-02 |
| 3.5e+01 | 5.2e+01 | 3.279369e+01 | 7.878273e-01 |
| 5.2e+01 | 7.0e+01 | 3.241271e+01 | 1.040042e+00 |
| 7.0e+01 | 8.8e+01 | 3.085981e+01 | 1.647534e-01 |
| 8.8e+01 | 1.0e+02 | 3.053596e+01 | 1.646319e-01 |
| 1.0e+02 | 1.2e+02 | 2.932101e+01 | 5.254681e-02 |
| 1.2e+02 | 1.4e+02 | 2.885834e+01 | 3.787400e-02 |
| 1.4e+02 | 1.2e+02 | 2.938429e+01 | 3.234589e-02 |
| 1.2e+02 | 1.0e+02 | 3.082386e+01 | 3.556911e-02 |
| 1.0e+02 | 8.8e+01 | 3.206172e+01 | 4.598714e-02 |
| 8.8e+01 | 7.0e+01 | 3.347215e+01 | 1.306014e-01 |
| 7.0e+01 | 5.2e+01 | 3.479778e+01 | 6.406369e-02 |
| 5.2e+01 | 3.5e+01 | 3.557331e+01 | 3.738005e-02 |
| 3.5e+01 | 1.8e+01 | 3.681176e+01 | 3.717942e-01 |
| 1.8e+01 | 0.0e+00 | 3.935902e+01 | 1.333740e-01 |